document.addEventListener("DOMContentLoaded", function(event) {
    const routes = [
        { path: '/', component: loginComponent },
        { path: '/view/:customerId', component: customerComponent, props: true},
    ];
    const router = new VueRouter({ routes });

    const app = new Vue({
        router,
        el: "#app",
        data: {
            shop_name: g.shop_name,
        }
    });
});
