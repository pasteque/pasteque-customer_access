g.products = {};
g.taxes = {};

var customerTemplate = `
        <div>
        <div v-if="customer_ready">
            <h2>{{ customer_info.dispName }}</h2>
            <p>customerKey: {{ customer_info.key }}</p>
            <p>Prepaid: {{ customer_info.prepaid | currency }}</p>
            <p>Debt: {{ customer_info.debt | currency }}</p>
        </div>
        <ul v-if="taxes_ready && products_ready && customer_tickets_ready">
            <li v-for="ticket in customer_tickets">
                <strong> Ticket n°{{ ticket.ticketId }} - {{ ticket.date | formatDate }}</strong>
                <ul>
                    <li v-for="line in ticket.lines">
                        {{ line.productId | productLabel }} x {{ line.quantity }} = {{ line.price * line.quantity * (1 + line.taxRate) | currency }}
                    </li>
                </ul>
                <ul>
                    <li v-for="payment in ticket.payments">
                        {{ payment.type }} : {{ payment.amount | currency }}
                    </li>
                </ul>
            </li>
        </ul>
        </div>`;

var customerComponent = {
    props: ['customerId'],
    data: () => {
        return {
            customer_info: {},
            customer_tickets: {},
            products: g.products,
            taxes: g.taxes,
            customer_ready: false,
            customer_tickets_ready: false,
            products_ready: false,
            taxes_ready: false,
        }
    },
    mounted: function() {
        this.fetchTaxes();
        this.fetchProductsInfo();
        this.fetchCustInfo();
    },
    methods: {
        fetchCustInfo: function() {
            this.$http.get(g.api_url + g.auth_string + '&p=CustomersAPI&action=get&id=' + this.customerId).then(
            response => {
                this.customer_info = response.body.content;
                if (this.customer_info === null) {
                    this.$router.replace('/');
                }
                this.customer_ready = true;
                this.fetchCustTickets();
            },
            error => {
                console.log('Customer not found !');
                this.$router.replace('/');
            })
        },
        fetchCustTickets: function() {
            this.$http.get(g.api_url + g.auth_string + '&p=TicketsAPI&action=search&customerId=' + this.customerId).then(
            response => {
                this.customer_tickets= response.body.content;
                // Tax rate fetching
                this.customer_tickets.forEach(function (ticket) {
                    ticket.lines.forEach(function (line) {
                        taxIndex = g.taxes.findIndex(function (tax, index) {
                            if(tax.id == line.taxId) {
                                return true;
                            }
                        });
                        console.log(taxIndex);
                        line.taxRate = g.taxes[taxIndex].taxes[0].rate;
                    });
                });
                this.customer_tickets_ready = true;
            },
            error => {
                console.log('Tickets of customer ' + customerId+ ' not found !');
            })
        },
        fetchProductsInfo: function() {
            this.$http.get(g.api_url + g.auth_string + '&p=ProductsAPI&action=getAll').then(
            response => {
                g.products = response.body.content;
                this.products_ready=true;
            },
            error => {
                console.log('Issue loading products');
            })
        },
        fetchTaxes: function() {
            this.$http.get(g.api_url + g.auth_string + '&p=TaxesAPI&action=getAll').then(
            response => {
                g.taxes = response.body.content;
                g.taxes.forEach(function (tax) {
                    tax.taxes[0].rate = parseFloat(tax.taxes[0].rate);
                });
                console.log(g.taxes);
                this.taxes_ready=true;
            },
            error => {
                console.log('Issue loading taxes');
            })
        }
    },
    filters: {
        currency: function (value) {
            value = parseFloat(value);
            if (isNaN(value)) {
                value = 0;
            }
            return value.toFixed(2) + " €";
        },
        formatDate: function (value) {
            var date = new Date(parseInt(value*1000));
            return date.getDate() + "/" + date.getMonth() + "/" + date.getFullYear();
        },
        productLabel: function(productId) {
            var index = g.products.findIndex(function(product) {
                return product.id == productId;
            });
            return g.products[index].label;
        }
    },
    template: customerTemplate
};

