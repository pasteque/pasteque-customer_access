var loginTemplate = `
    <form @submit.prevent="login">
        <div>Mon id est: <input placeholder="votre id client" type="text" v-model="customer_key"></div>
        <button type="submit">LOGIN</button>
    </form>
`;

var loginComponent = {
    data: () => {
        return {
            customer_key: ''
        }
    },
    methods: {
        login: function() {
            this.$router.replace('view/' + this.customer_key);
        }
    },
    template: loginTemplate,
}
